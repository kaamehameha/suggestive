package main

/*
Problem :
In the keypad of old mobile phones each number from 1-9 was mapped to multiple
alphabetic characters. Given a series of numbers, and a dataset of known words,
provide suggestions(from the dataset of known words) to those series of numbers
such that the numbers spell out the words if pressed on the phone.

Eg:
if numpad was such -
1 : "ABC",
2 : "DEF",
3 : "GHI" ...etc

Given data set contains names like ("abacus","acumen", "blastoise"),
Typing in "11", should suggest "abacus" and "acumen".
Typing in "95" should suggest "blastoise"
*/

import (
	"fmt"
	"github.com/urfave/cli"
	"os"
	"strings"
	lib "suggestive/lib"
)

func main() {
	app := cli.NewApp()

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "vocabFile",
			Value: "./assets/vocabFile.csv",
			Usage: "Vocabulary file path",
		},
		cli.IntFlag{
			Name:  "limit",
			Value: 30,
			Usage: "Number of suggestions limit",
		},
	}
	app.Action = func(c *cli.Context) error {
		numStr := strings.TrimSpace(c.Args().Get(0))
		fmt.Println()

		// This must be from a file
		vocabulary, ok := lib.ReadVocabFromFile(c.String("vocabFile"))
		if ok != nil {
			return cli.NewExitError("Could not read from vocab file", 0)
		}
		// Pre processing
		lib.ProcessVocabulary(vocabulary)

		if len(numStr) == 0 {
			// Should be a usage error here
			return cli.NewExitError("Input number string is mandatory", 0)
		}

		suggestions := lib.GetSuggestions(numStr)
		if len(suggestions) > 0 {
			// Truncate the limit here
			limit := c.Int("limit")
			fmt.Printf("Suggestions for \"%s\" are - \n\n", numStr)
			for i, sugg := range suggestions {
				fmt.Println(sugg)
				if i > limit {
					break
				}
			}
		} else {
			fmt.Printf("There are no suggestions for %s \n", numStr)
		}

		return nil
	}
	app.Run(os.Args)
}
