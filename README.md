## Suggestive
In the keypad of old mobile phones each number from 1-9 was mapped to multiple
alphabetic characters. Given a series of numbers, and a dataset of known words,
provide suggestions(from the dataset of known words) to those series of numbers
such that the numbers spell out the words if pressed on the phone.

Eg:
if numpad was such -
1 : "ABC",
2 : "DEF",
3 : "GHI" ...etc

Given data set contains names like ("abacus","acumen", "blastoise"),
Typing in "11", should suggest "abacus" and "acumen".
Typing in "95" should suggest "blastoise"

Examples of running this from the commandline - 

```
$ suggestive 92
Suggestions for "92" are - 

zeal
```

With args -
`suggestive -h` is useful.

Adding a different vocabulary file and imposing a limit
```
$ suggestive --vocabFile assets/otherVocabFile.csv --limit 30 111

Suggestions for "111" are - 

abbey
abbie
abby
babara
babette
```
There are a couple of vocabulary files in `assets/` directory.
A single file version of this code is also in [Go playground](https://play.golang.org/p/iHKGR6BwK_)
