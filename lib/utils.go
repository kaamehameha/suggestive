package lib

import (
	"bufio"
	_ "fmt"
	"os"
	"strings"
)

func GetKeypadNumForChar(c int) int {
	// This is an assumption
	// Can have a simple map for more involved stuff
	return (((int(c) - int('a')) + 3) / 3)
}

func GetKeypadSeriesForName(name string) []int {
	var numSeries []int
	for _, c := range name {
		val := GetKeypadNumForChar(int(c))
		//fmt.Printf("%c - %d\n", c, val)
		numSeries = append(numSeries, val)
	}
	return numSeries
}

func ReadVocabFromFile(filePath string) ([]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, strings.ToLower(scanner.Text()))
	}
	return lines, scanner.Err()
}
