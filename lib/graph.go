package lib

import (
	_ "fmt"
	"strings"
)

// This is the basic component of the graph.
type Node struct {
	matchedWords []*string
	nextNodeMap  map[int]*Node
}

var root *Node

func createNewNode() *Node {
	return &Node{nextNodeMap: make(map[int]*Node)}
}

func ProcessVocabulary(vocabulary []string) {
	root = createNewNode()

	for i, _ := range vocabulary {
		//fmt.Println("Calling addWord to ", word)
		//fmt.Println("Conversion - ", GetKeypadSeriesForName(word))
		addWordToGraph(&vocabulary[i])
		//fmt.Println(root)
	}
}

// The word is added to the graph iteratively
func addWordToGraph(wordPtr *string) {
	var nodeAddr *Node

	// Adding the word to the matched word in the root
	nodeAddr = root

	for _, char := range *wordPtr {
		nodeAddr.matchedWords = append(
			nodeAddr.matchedWords,
			wordPtr,
		)
		keypadNum := GetKeypadNumForChar(int(char))
		nextNode, ok := nodeAddr.nextNodeMap[keypadNum]
		if !ok {
			// Create a new node and add it existing node
			nextNode = createNewNode()
			nodeAddr.nextNodeMap[keypadNum] = nextNode
		}
		nodeAddr = nextNode
	}
}

// This method takes in a string of numbers,
// and traverses the graph from the root node.
// If a path doesn't exist before all the numbers in
// formal param have been traversed, failure is returned,
// and not a partial match.
func GetSuggestions(keypadNumbers string) []string {
	var suggestions []string

	keypadNumbers = strings.TrimSpace(keypadNumbers)
	if len(keypadNumbers) == 0 {
		return suggestions
	}

	// Convert the numStr to an array
	nodeAddr := root
	for _, num := range keypadNumbers {
		numVal := int(num) - int('0')

		// In the root, find the numVal if listed in the map
		nextNodeAddr, ok := nodeAddr.nextNodeMap[numVal]
		if !ok {
			// Matches failed before all the values had a chance
			nodeAddr = nil
			break
		}
		nodeAddr = nextNodeAddr
	}
	if nodeAddr != nil && nodeAddr != root {
		for _, wp := range nodeAddr.matchedWords {
			suggestions = append(suggestions, *wp)
		}
	}
	return suggestions
}
